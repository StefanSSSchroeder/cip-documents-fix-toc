# CIP Security Partitions

[[_TOC_]]

*****
<div style='page-break-after: always'></div>

## Revision History
| Revision No | Date       | Change description       | Author          | Reviewed by                               |
|-------------|------------|--------------------------|-----------------|-------------------------------------------|
| 001         | 2021-11-29 | first draft              | Raphael Lisicki | To be reviewed by CIP Security WG members |
| 002         | 2024-05-28 | Replace manual TOC with automatic TOC                        | Stefan Schroeder | Dinesh Kumar |


<div style='page-break-after: always'></div>

***

## 1. Objective <a name="Objective"></a>

The primary objective of this document is to explain how the integrity and confidentiality of files stored on a device is maintained.


## 2. Partitions <a name="Partitions"></a>

The following partition scheme is meant to illustrate the security capabilities. Details regarding software update and hardware architecture remain in the realm of the respective working groups. A x86 system using the A/B partition scheme for updates is assumed for this illustration.
The shown solutions provides integrity for the system partition and confidentiality for user data.

![Partitions](../resources/images/other_documents/partitions-security.jpg)


