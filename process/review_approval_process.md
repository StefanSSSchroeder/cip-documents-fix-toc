# <Center>Roles and Responsibilities</Center>

[[_TOC_]]

   ***** 
<div style='page-break-after: always'></div>

## Revision History
| Revision No | Date       | Change description                                          | Author       | Reviewed by |
|-------------|------------|-------------------------------------------------------------|--------------|-------------|
| 001         | 2024-01-29 | Draft review & approval process for CIP development records | Dinesh Kumar | Stefan      |
| 002         | 2024-05-23 | Fixed review comments provided by Stefan                      | Dinesh Kumar | TBR         |
| 003         | 2024-05-28 | Replace manual TOC with automatic TOC                        | Stefan Schroeder | Dinesh Kumar |

****
<div style='page-break-after: always'></div>


<div style='page-break-after: always'></div>

***

## 1. Objective 

The primary objective of this document is to define repeatable process for CIP development process records.
CIP being a platform based on Open Source Components where multiple developers contribute who come from different backgrounds, hence the process is generic and tries to use various tools for review and approval.

## 2. Scope 

The scope of this document is artifacts, like code, scripts, documents, developed by all CIP work-group members

## 3. Review and Approval Process 

CIP produces various types of artifacts and depending upon the type of artifact review and approval processes vary.

Some of the artifacts like workgroup meeting agenda, general technical discussion etc don't undergo any review and approval process.

The following sections define the review and approval process followed by CIP working groups for development artifacts

### 3.1 Documents 

  All development process documents have a revision history. Changelogs are maintained. Changeloge include modification date and creation date, reviewer, etc. All documents are maintained in Gitlab. This guarantees the availability of complete, correct and consistent changelogs. Changelogs are derived using the "git log" functionality.
  
  Following are the steps followed for review and approval of development documents.
  
  **Step-1** Any CIP member can create draft documents and share it for CIP members review via Gitlab merge requests (MR).
  
  **Step-2** Related members share feedback and the owner of the document resolves all comments.
  
  **Step-3** Once all comments are resolved the maintainer of the cip-documents repository will merge the document, once the document is merged with no pending review comments, it is considered as approved by the reviewer.
  
  Any further change follows same process and revision history updates.

### 3.2 Code 

CIP reuses upstream components and integrates them for creating CIP reference images. CIP members write very minimal code, scripts and configuration. All types of code changes follow the process outlined below.

**Step-1** Anyone can create patches in different CIP workgroups and share for CIP members review via mailing lists.

**Step-2** Members of the respective mailing lists review the changes and share feedback, contents of mailing lists are             archived. 

**Step-3** Owner of the patch completes rework to fix all review comments and makes required changes in the patch. Once all review                   comments are resolved, respective maintainer of the repository accepts the patches. However, in case if the owner of the patch 
           and maintainer is same, patch owner can merge his patches as all maintainers are experienced members and explain to working group members.

All the change log history is maintained in Gitlab commit logs.

### 3.3 Other Artifacts 

CIP produces several other types of artifacts like working group agenda documents, investigation documents, presentations at various Open Source Software (OSS) events etc. These documents don't follow any review and approval process.
