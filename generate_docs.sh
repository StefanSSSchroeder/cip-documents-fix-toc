#!/bin/bash
# This script generates PDF documents from all Markdown files in this
# repository using Pandoc and xeLaTeX.
# Written by Stefan Schroeder for the CIP project.
# (C) CIP project 2022

TEMPLATE=../resources/pandoc-template.tex
METADATA=../resources/pandoc-pre.yml
ENGINE=xelatex

echo "$0: Starting"
mkdir -p output/
# Cleanup from previous runs
rm -f output/*.md output/*.pdf

# Special case for top-level file
#cp README.md output/
#cp user/user_manual/user_manual.md output/
#cp testing/core/upstream_test_results.md output/

#files_to_process=README.md process/*.md security/*.md testing/*.md user/user_manual/*.md
#files_to_process=security/iec62443-4-2-FR-[123].md
#files_to_process=user/user_manual/user_manual.md
files_to_process=$(find * -name '*.md' ! -path "output/*" -type f -print)

# echo $files_to_process

# Process all Markdown files in subdirectories.
for i in $files_to_process; do
	echo "============================"
	echo "-- $i : Starting"
	b0=$(dirname $i)
	b1=$(basename $i .md)
	out_md="${b0}_${b1}.md"
	out_pdf="${b0}_${b1}.pdf"
	cp $i output/$out_md
	pushd output > /dev/null

	# Remove <Center> and </Center> header tags
	sed -i -E 's/<.?Center>//g' $out_md
	# Remove TOC tag.
	# In Gitlab, the [[_TOC_]] displays the table-of-content.
	# In PDF, the pandoc 'toc' feature is used to autogenerate a table.
	sed -i -E 's/.._TOC_..//g' $out_md

	pandoc $out_md --from gfm --template=$TEMPLATE --pdf-engine=$ENGINE --metadata-file=$METADATA --output $out_pdf
	result="Fail"
	[ -f $out_pdf ] && result="Success"
	echo "-- $i : Done $result" | tee -a "summary.log"
	popd > /dev/null
	echo "============================"
done
echo "$0: Done"

