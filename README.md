# CIP Documents

[[_TOC_]]

This repository is where keeps all documents at one place for all working 
groups of the CIP projects to meet secure development process definced at 
IEC 62443-4-1 which require to maintain documents and their versions.
 
## Management policy

This repository will be maintained by a few security members to meet 
secure development process, thus branches in this repository will be 
protected by restricting members enabling to push and merge.  

## License

The license of all documentation in this repository follows 
the intellectual property policy in the CIP Charter. 
See section 14-e in [the CIP Charter](https://www.cip-project.org/about/charter).

## Guide

This section will give brief descriptions about each document to make navigating this repository easier. Non-document files will not be explained here.
- cip-project
  - cip-documents
    - developer
    - event
    - process
    - security
    - testing
    - user

### Developer

|Name|Description|
|--|--|
|[FOSS_Security_Study_Summary](https://gitlab.com/cip-project/cip-documents/-/blob/master/developer/investigation/FOSS_Security_Study_Summary.pdf)|Presentation on security increases in Debian over time.|

### Event

|Name|Description|
|--|--|
|[Introduction of CIP Software Updates Working Group](https://gitlab.com/cip-project/cip-documents/-/blob/master/event/2019/sw_updates_wg_mini-summit.pdf)|Presentation CIP Software Update WG.|
|[CIP Security towards achieving industrial grade security](https://gitlab.com/cip-project/cip-documents/-/blob/master/event/2020/CIP_Security_WG_OSSNA_r02.pdf)|Presentation CIP Security WG.|
|[Threat modelling - Key methodologies and applications from OSS CIP(CIP) perspective](https://gitlab.com/cip-project/cip-documents/-/blob/master/event/2020/%5BELCE%5D%20Threat%20modelling%20-%20Key%20methodologies%20and%20applications%20from%20OSS%20CIP(CIP)%20perspective.pdf)|Presentation of CIP Security WG on Threat modeling in CIP.|

### Process

|Name|Description|
|--|--|
|[CIP File Integrity](https://gitlab.com/cip-project/cip-documents/-/blob/master/process/file_integrity.md)|The primary objective of this document is to explain about how file integrity for CIP deliverables is achieved.|
|[CIP Roles and Responsibility Matrix](https://gitlab.com/cip-project/cip-documents/-/blob/master/process/raci.md)|The primary objective of this document is to show the roles in CIP with their responsibilities and accountabilities. It is also shwon which roles should be consulted and/or informed for certain actions and which qualifications, if any, are needed to fulfill a role.|
|[CIP Secure Development Process](https://gitlab.com/cip-project/cip-documents/-/blob/master/process/secure_development_process.md)|This document is based on IEC-62443-4-1 (Edition 1.0 2018-01) secure development process requirements.The Objective is to adhere IEC-62443-4-1 secure development process requirements in CIP development as much as possible.|

### Security

|Name|Description|
|--|--|
|[CIP Security Coding Guide Lines](https://gitlab.com/cip-project/cip-documents/-/blob/master/security/CIP-Security-CodingGuideLines.md)|This document explains how CIP Project and its upstream projects are following security coding guidelines.|
|[Static analysis tools for CIP packages](https://gitlab.com/cip-project/cip-documents/-/blob/master/security/CIP-Security-StaticAnalysisTools.md)|This document explains how CIP Project executes SCA with some explanation on how to use some SCA software.|
|[CIP Development Environment Security](https://gitlab.com/cip-project/cip-documents/-/blob/master/security/development_environment_security.md)|The primary objective of this document is to document current development environment security, development flow and how security is maintained.|
|[IEC 62443-4-2 App & HW Guidelines](https://gitlab.com/cip-project/cip-documents/-/blob/master/security/iec62443-app-hw-guidelines.md)|The primary objective of this document is to provide guidelines to CIP users for meeting IEC-62443-4-2 security requirements. The document explains about each IEC-62443-4-2 requirements whether it has already been met by CIP. In addition this document also explains about iec security layer added in CIP to meet IEC-62443-4-2 security requirements.|
|[User Security Manual](https://gitlab.com/cip-project/cip-documents/-/blob/master/security/user_security_manual.md)|This document contains items identified during IEC-62443-4-1 and IEC-62443-4-2 Gap Assessment for user security manual.|
|[OWASP Top 10 Vulnerabilities Monitoring](https://gitlab.com/cip-project/cip-documents/-/blob/master/security/owasp_top10_vulnerabilities_monitoring.md)|The primary objective of this document is to explain about how various OWASP. top 10 vulnerabilities are handled in CIP.|
|[CIP Private Key Management](https://gitlab.com/cip-project/cip-documents/-/blob/master/security/private_key_management.md)|The primary objective of this document is to explain about how various private keys used in CIP development are maintained and kept secure and confidential.|
|[CIP Security Requirements](https://gitlab.com/cip-project/cip-documents/-/blob/master/security/security_requirements.md)|This document is intended to capture CIP security requirements based on IEC-62443-4-2 standard.|
|[CIP Threat Modeling](https://gitlab.com/cip-project/cip-documents/-/blob/master/security/threat_modelling.md)|The primary objective of this document is to create Threat Model for CIP reference platform.|

### Testing

|Name|Description|
|--|--|
|[CIP_IEC-62443-4-2_Security_TestCases](https://gitlab.com/cip-project/cip-documents/-/blob/master/testing/core/iec-62443-evaluation/CIP_IEC-62443-4-2_Security_TestCases_Rev03.xlsx)|Overview of the CIP 62443-4-2 test cases.|
|[CIP Penetration Testing](https://gitlab.com/cip-project/cip-documents/-/blob/master/testing/CIP-PenetrationTesting.md)|The primary objective of this document is to identify suitable penetration testing tool and document the process how this can be re-used by CIP end users for their specific use cases.|

### User

|Name|Description|
|--|--|
|[CIP User Manual](https://gitlab.com/cip-project/cip-documents/-/blob/master/user/user_manual/user_manual.md)|This document is a user perspective overview and technical guide for CIP.|
